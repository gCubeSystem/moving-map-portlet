
# Changelog for MOVING Multi-Actor Platform Portlets

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.0.0-SNAPSHOT] - 2021-11-02

First Release
