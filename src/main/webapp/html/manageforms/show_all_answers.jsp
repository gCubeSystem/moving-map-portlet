<%@include file="../init.jsp"%>
<%
FilledForm filledForm = (FilledForm) request.getAttribute("filledForm");

String copySubmitted = new StringBuffer()
.append("<br>").append("<b>Name: </b>")
.append(filledForm.getName()).append(" ").append(filledForm.getSurname())
.append("<br>").append("<b>Organisation: </b>")
.append(filledForm.getOrganisation())
.append("<br>").append("<b>Type of Organisation/Stakeholder: </b>")
.append(filledForm.getOrganisationTyp())
.append("<br>").append("<b>e-mail Address: </b>")
.append(filledForm.getEmailAddress())
.append("<br>").append("<b>Country: </b>")
.append(filledForm.getCountry())
.append("<br><br>").append("<b>Have you participated in the activities of any of our MOVING Regional Multi-Actors Platforms? </b>")
.append(filledForm.isParticipatedInActivities())
.append("<br><br>").append("<b>What is your main motivation for joining the EU MAP of MOVING? </b>")
.append(filledForm.getMainMotivation())
.append("<br><br>").append("<b>Please elaborate on your motivation expressed above so that we understand better your interest, and form a dynamic and relevant community. Vague explanations of the motivation might not be taken into consideration. </b>")
.append("<br><br>").append(filledForm.getTextareaMotivation())
.append("<br><br>").append("<b>What is your main area of expertise in relation to mountain sustainability and resilience? </b>")
.append(filledForm.getAreaOfExpertise())
.append("<br><br>").append("<b>Please elaborate on the relevant experience you can bring to mountain value chains and the resilience of these areas, so that we have more detailed information to be able to form a dynamic and relevant community: </b>")
.append("<br><br>").append(filledForm.getElaborated_expertise())
.append("<br><br>").append("<b>As a starting point, the degree of participation you commit to is: </b>")
.append(filledForm.getDegree_of_participation())
.append("<br>").append("<br>").append("<b>Data Management: </b>")
.append("Agreed").toString();


pageContext.setAttribute("copySubmitted", copySubmitted);

%>
<a class="btn btn-link btn-large no-padding"
	href="javascript: history.back();"><i class="icon icon-angle-left"></i>&nbsp;Back</a>
<div class="form-display"><p style="padding: 25px;">${copySubmitted}<p></div>
<a class="btn btn-link btn-large no-padding"
	href="javascript: history.back();"><i class="icon icon-angle-left"></i>&nbsp;Back</a>