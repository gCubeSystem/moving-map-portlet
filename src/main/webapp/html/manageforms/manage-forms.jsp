<%@include file="../init.jsp"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>


<%
List<FilledForm> filledForms = (List<FilledForm>) renderRequest.getAttribute("filledForms");
pageContext.setAttribute("filledForms", filledForms);
%>

<portlet:actionURL var="showAnswersURL" name="showAnswers"></portlet:actionURL>
<portlet:actionURL var="rejectApplicationURL" name="rejectApplication" />

<portlet:resourceURL var="acceptApplicationURL"></portlet:resourceURL>

<script>

function closeAndReload() {
	$("#modalSuccess").hide();
	location.reload();
}

function getSelectedRadioFormId() {
	if (document.querySelector('input[name="applicationItem"]:checked') == null)
		alert('Please select the form you wish to operate with first');
	return document.querySelector('input[name="applicationItem"]:checked').value;
}


function acceptForm(endpoint, applicationItemId) {
	console.log("acceptForm:" + endpoint);
	$("#acceptButton").prop("disabled",true);
	$("#acceptButton").text('Please wait ...');
	$.ajax({
		url : endpoint,
		type : 'POST',
		datatype : 'json',
		data : {
			applicationItemId : applicationItemId,
			userId : Liferay.ThemeDisplay.getUserId(),
			groupId : Liferay.ThemeDisplay.getScopeGroupId()
		},
		success : function(data) {
		
			var content = JSON.parse(data);
			if (content.success == "OK") {
				$("#modalSuccess").show();
			}
			$("#acceptButton").prop("disabled",false);
			$("#acceptButton").text('Accept');
		}
	});
}
	
function performAction(action) {
	if (document.querySelector('input[name="applicationItem"]:checked') == null) {
		alert('Please select the filled form first');
		return false;
	}
	if (action == 0) {
		//document.getElementById("form_answers").submit();// Form submission
		document.getElementById("form_answers").action = "<%=showAnswersURL%>";
		document.getElementById("form_answers").submit();// Form submission
	 }
	 else if (action == 1) {
		if (confirm("Are you sure to REJECT the selected Expression of Interest?")) {			
			document.getElementById("form_answers").action = "<%=rejectApplicationURL%>";
			document.getElementById("form_answers").submit();// Form submission
		}
	 }
     else {
    	 if (confirm("Are you sure to ACCEPT the selected Expression of Interest?")) {		
    		 acceptForm('${acceptApplicationURL}', getSelectedRadioFormId());
    	 }
	}
	
}

</script>



<div style="text-align: center; padding: 25px;">

	<form method="post" id="form_answers" name="fm">
		<table id="example" class="paleBlueRows">
			<tbody>
			<thead>
				<tr>
					<th>Sel.</th>
					<th>Full name</th>
					<th><span title="Email" style="margin-left: 10px;">Email</span></th>
					<th><span title="Country" style="margin-left: 10px;">Country</span></th>
					<th><span style="margin-left: 10px;">Organisation</span></th>
				</tr>
			</thead>
			<c:forEach var="form" items="${filledForms}">
				<tr>
					<td><input type="radio" name="applicationItem"
						value="${form.id}" id="${form.id}" style="margin: 4px 5px 10px;"></td>
					<td><span
						style="margin-left: 10px; margin-right: 10px; color: #666;">${form.name}
							${form.surname}</span></td>
					<td><span
						style="margin-left: 10px; margin-right: 10px; color: #666;">${form.emailAddress}</span></td>
					<td><span
						style="margin-left: 10px; margin-right: 10px; color: #666;">${form.country}</span></td>
					<td><span
						style="margin-left: 10px; margin-right: 10px; color: #666;">${form.organisation}</span></td>
				</tr>
			</c:forEach>
			</tbody>
		</table>

		<div style="width: auto; text-align: right; padding: 25px;">
			<button id="accept" type="button" class="btn btn-large"
				onClick="performAction(0)">See answers</button>
			<button class="btn btn-large btn-warning" type="button" onClick="performAction(1)">Reject</button>
			<button id="acceptButton" class="btn btn-large btn-primary" type="button" onClick="performAction(2)">Accept</button>
		</div>
	</form>
</div>

<div id="modalSuccess" class="modal" style="display: none;">
	<p class="lead">
		The user has been added to the VRE correctly and informed via email.<br>
		Please <a href="#" onClick="closeAndReload();">click here</a> to refresh the page.
	</p>
</div>