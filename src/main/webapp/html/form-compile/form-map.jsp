<%@include file="../init.jsp"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>


<%
List<String> organisation_types = (List<String>) renderRequest.getAttribute("organisation_types");
List<String> main_motivations = (List<String>) renderRequest.getAttribute("main_motivations");
List<String> areas_of_expertise = (List<String>) renderRequest.getAttribute("areas_of_expertise");
List<String> degrees_of_participation = (List<String>) renderRequest.getAttribute("degrees_of_participation");

pageContext.setAttribute("organisation_types", organisation_types);
pageContext.setAttribute("main_motivations", main_motivations);
pageContext.setAttribute("areas_of_expertise", areas_of_expertise);
pageContext.setAttribute("degrees_of_participation", degrees_of_participation);

pageContext.setAttribute("required", "true");
%>
<portlet:defineObjects />
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


<portlet:actionURL var="validateURL" name="submittedForm" />


<p class="lead">Expression of Interest for the EU Multi-Actor
	Platform</p>
<aui:form action="<%=validateURL%>" method="post" name="fm"
	id="movingForm">
	<aui:fieldset>
		<aui:input label="Name" name="name" type="text" required="${required}"
			style="width:300px;" />
		<aui:input label="Surname" name="surname" type="text"
			required="${required}" style="width:300px;" />
		<aui:input label="Organisation" name="organisation" type="text"
			required="false" style="width:300px;" />
		<!-- 
		SELECT
		 -->
		<aui:select label="Type of Organisation/Stakeholder "
			name="organisationType" required="${required}" showEmptyOption="true"
			style="width:800px;">
			<c:forEach var="type" items="${organisation_types}">
				<aui:option value="${type}">${type}</aui:option>
			</c:forEach>
		</aui:select>
		<aui:input name="emailAddress" required="${required}"
			style="width:300px;">
			<aui:validator name="email" />
		</aui:input>
		<aui:input label="Country" name="country" type="text"
			required="${required}" style="width:300px;" />
	</aui:fieldset>
	<aui:fieldset>

		<aui:field-wrapper name="participatedInActivitiesWrapper"
			label=" Have you participated in the activities of any of our MOVING Regional Multi-Actors Platforms? (Required)">

			<div class="control-group input-text-wrapper">
				<aui:input label="Yes" name="participatedInActivities" type="radio"
					value="true" />
				<aui:input label="No" name="participatedInActivities" type="radio"
					value="false" checked="true" />
			</div>

		</aui:field-wrapper>
		<div class="control-group input-text-wrapper">
			<aui:field-wrapper name="mainMotivation"
				label="What is your main motivation for joining the EU MAP of MOVING? (Required)">
				<!-- the first is needed as placeholder otherwise the validator does not work, make sure you take into account in the server part -->
				<aui:input id="mainMotivation" type="checkbox" name="mainMotivation"
					cssClass="first-checkbox-hidden" label="" />
				<c:forEach var="type" items="${main_motivations}">
					<aui:input id="mainMotivation" type="checkbox"
						name="mainMotivation" label="${type}" />
				</c:forEach>
			</aui:field-wrapper>
		</div>
		<aui:spacer></aui:spacer>

		<aui:input
			label="Please elaborate on your motivation expressed above so that we understand better your interest, and form a dynamic and relevant community. Vague explanations of the motivation might not be taken into consideration."
			name="textareaMotivation" type="textarea" style="width:800px;"
			placeholder="Free text 150 words" />

		<!-- 
		 Multi areas_of_expertise
		 -->

		<div class="control-group input-text-wrapper">
			<aui:field-wrapper name="areaOfExpertiseSelect"
				label="What is your main area of expertise in relation to
				mountain sustainability and resilience? (Required)">
				<!-- the first is needed as placeholder otherwise the validator does not work, make sure you take into account in the server part -->
				<aui:input id="areaOfExpertiseSelect" type="checkbox"
					name="areaOfExpertiseSelect" cssClass="first-checkbox-hidden"
					label="" />
				<c:forEach var="type" items="${areas_of_expertise}">
					<aui:input id="areaOfExpertiseSelect" type="checkbox"
						name="areaOfExpertiseSelect" label="${type}"
						showRequiredLabel="false" />
				</c:forEach>
			</aui:field-wrapper>
		</div>
		<aui:spacer></aui:spacer>

		<aui:input
			label="Please elaborate on the relevant experience you can bring to mountain value chains and the resilience of these areas, so that we have more detailed information to be able to form a dynamic and relevant community."
			name="textareaExperience" type="textarea" style="width:800px;"
			placeholder="Free text 150 words" />

		<div class="control-group input-text-wrapper">
			<aui:field-wrapper name="commitment"
				label="As a starting point, the degree of participation you commit to is: (Required)">
				<!-- the first is needed as placeholder otherwise the validator does not work, make sure you take into account in the server part -->
				<aui:input id="commitment" type="checkbox" name="commitment"
					cssClass="first-checkbox-hidden" label="" />
				<c:forEach var="type" items="${degrees_of_participation}">
					<aui:input id="commitment" type="checkbox" name="commitment"
						label="${type}" />
				</c:forEach>
			</aui:field-wrapper>
		</div>

		<div style="margin-top: 20px;">&nbsp;</div>
		<p class="lead">Data Management</p>
		<p>
			MOVING is responsible for the processing of the personal data
			provided with your consent when registering and informs you that the
			data provided will be processed in accordance with the General Data
			Protection Regulation (EU) 2016/679 of 27 April 2016 (GDPR) and other
			applicable rules, for the management of the activity and to send
			other electronic communications of content related to it. Read more
			in our <a href="https://www.moving-h2020.eu/privacy-cookies-policy/"
				target="_blank">Privacy Policy</a> .
		</p>
		<aui:input id="privacyPolicyAgree" type="checkbox"
			name="privacyPolicyAgree" label=" I Agree" required="${required}" />
	</aui:fieldset>
	<!-- 
		Submit Button
		 -->
	<div style="width: 100%; text-align: center;">
		<div class="g-recaptcha"
			data-sitekey="6Ld0hfYZAAAAAKlIHde1v3QE4NyY4AniN6j-U9SP"></div>
		<br />
		<aui:button-row>
			<aui:button value="Submit your application" type="submit"
				cssClass="btn-large btn-fill" />
		</aui:button-row>
	</div>

</aui:form>


<!-- Validate the captcha -->
<script type="text/javascript">
	AUI().use('aui-tooltip', 'aui-base', 'selector-css3', function(A) {

		var form = A.one('#<portlet:namespace />fm');
		if (form) {
			form.on('submit', function(event) {
				if (grecaptcha.getResponse() === '') {
					event.halt();
					event.stopImmediatePropagation();
					alert('Please prove you are not a robot');
				}
			});
		}
	});
</script>


<!-- Validate the checkboxes, the number of min oprtions is configurable -->
<aui:script>
	AUI().use('aui-form-validator', 'aui-overlay-context-panel', function(A) {

		// Extending Alloy Default values for FormValidator RULES
		var DEFAULTS_FORM_VALIDATOR = A.config.FormValidator;
		A.mix(
  			DEFAULTS_FORM_VALIDATOR.RULES,
			{
				requiredCheckbox:function (val, fieldNode, ruleValue) {
					var counter = 0;
					var nodeName = fieldNode.attr('name');
					var checkBoxes = A.all("input[name='" + nodeName + "']").val();
					for (var i = 0, len = checkBoxes.length; i < len; i++ ) {
						if (checkBoxes[i] == 'true') { 
							counter++;
						};
					}
					console.log('counter='+counter);
					return counter >= ruleValue;
				},
			},
			true
		);

		// Extending Alloy Default values for FormValidator STRINGS
		A.mix(
			DEFAULTS_FORM_VALIDATOR.STRINGS,
			{
				requiredCheckbox: '<liferay-ui:message
		key="Please select at least one of the following options: " />'
			},
			true
		);

		// Specify the form validation rules to be applied on this form
		var rules = {
			<portlet:namespace />areaOfExpertiseSelect: {
				requiredCheckbox: 1
			},
			<portlet:namespace />mainMotivation: {
				requiredCheckbox: 1
			},
			<portlet:namespace />commitment: {
				requiredCheckbox: 1
			}						
	    };

		// filedStrings to override the standard error msgs
	    var fieldStrings = {
	    };

	    new A.FormValidator(
	      {
	        boundingBox: '#<portlet:namespace />fm',
	        fieldStrings: fieldStrings,
	        rules: rules,
	        showAllMessages: true
	      }
	    );
	});		
</aui:script>


