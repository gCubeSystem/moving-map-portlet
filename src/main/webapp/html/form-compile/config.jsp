<%@include file="../init.jsp"%>

<liferay-portlet:actionURL portletConfiguration="true"
	var="configurationURL" />

<%
	String VREGroupId_cfg = GetterUtil.getString(portletPreferences.getValue("VREGroupId", StringPool.BLANK));

%>

<aui:form action="<%=configurationURL%>" method="post" name="fm">
	<aui:input name="<%=Constants.CMD%>" type="hidden"
		value="<%=Constants.UPDATE%>" />

	<!-- Application URL -->
	<aui:field-wrapper cssClass="field-group">
		<aui:input style="width: 100%;" name="preferences--VREGroupId--"
			type="text" cssClass="text long-field" showRequiredLabel="true"
			label="VRE GroupId" inlineField="true" inlineLabel="left"
			placeholder="12345"
			helpMessage="Il groupId della VRE dove cercare i VRE Managers da notificare quando nuove form"
			value="<%=VREGroupId_cfg%>" required="true" />
	</aui:field-wrapper>

	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>

</aui:form>