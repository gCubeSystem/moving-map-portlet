package org.gcube.portlets.user.moving;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.user.moving.createaccounts.CreateUserAccount;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ManageForms
 */
public class ManageForms extends MVCPortlet {
	private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(ManageForms.class);

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse) {
		Connection conn = null;
		try {
			conn = DatabaseConnection.getInstance("/"+PortalContext.getConfiguration().getInfrastructureName()).getConnection();
			_log.info("Trying getting FORMS from DB");
			List<FilledForm> filledForms = getNonProcessedForms(conn);
			_log.info("Succesfully got Non Processed Form from DB, forms found: " +filledForms.size());
			renderRequest.setAttribute("filledForms", filledForms);
			super.render(renderRequest, renderResponse);		
		} catch (Exception e) {
			_log.error("Something wrong with Databaseconnection or with getting FORMS from DB", e);
		}
	}

	public static String getCurrentContext(long groupId) {
		try {
			PortalContext pContext = PortalContext.getConfiguration(); 
			return pContext.getCurrentScope(""+groupId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * used to accept the application
	 */
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		String formId = ParamUtil.getString(resourceRequest, "applicationItemId", null);
		System.out.println("\n\n *** formId="+formId);
		Connection conn = null;
		try {
			conn = DatabaseConnection.getInstance("/"+PortalContext.getConfiguration().getInfrastructureName()).getConnection();
			FilledForm filledForm = getFormById(conn, formId);
			_log.info("filledForm exists Trying UPDATE FORM ON DB");
			boolean user_accepted = true;
			setFormProcessedAndStateById(conn, formId, user_accepted);
			_log.info("UPDATED FORM ON DB to Accepted, formId="+formId);
			try {
				_log.info("Sending email to user");
				Properties props = System.getProperties();
				Session session = null;
				props.put("mail.smtp.host", MConstants.MAIL_SERVICE_HOST);
				props.put("mail.smtp.port", MConstants.MAIL_SERVICE_PORT);
				//use localhost (probaly postfix instance)
				session = Session.getDefaultInstance(props);
				Message message = new MimeMessage(session);
				message.setFrom(new InternetAddress(CompileForm.MAIL_FROM));
				message.setRecipients(
						Message.RecipientType.TO, InternetAddress.parse(filledForm.getEmailAddress()));
				message.setSubject(MConstants.MAIL_SUBJECT_FEEDBACK);
				message.setText(MConstants.MAIL_BODY_ACCEPT);
				// Send message
				Transport.send(message);
			}	catch (Exception e) {
				_log.error("Something wrong with sending email", e);
			}	
			_log.info("Sending acceptance email done, adding user to the VRE and creating account if needed");
			HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest));

			CreateUserAccount.register(
					httpRequest, 
					filledForm.getName(), 
					filledForm.getSurname(), 
					filledForm.getOrganisation(), 
					filledForm.getEmailAddress(),
					true,
					true, filledForm.getDateCreated());

			_log.info("acceptApplication completed");

		} catch (Exception e) {
			_log.error("Something wrong with Database connection or with getting FORMS from DB", e);
		}	
		JSONObject fileObject = JSONFactoryUtil.createJSONObject();
		fileObject.put("success", "OK");
		resourceResponse.getWriter().println(fileObject);
	}

	public void rejectApplication(ActionRequest request, ActionResponse actionResponse) throws IOException, PortletException {
		_log.info("rejecting form");
		String formId = ParamUtil.getString(request, "applicationItem");
		Connection conn = null;
		try {
			conn = DatabaseConnection.getInstance("/"+PortalContext.getConfiguration().getInfrastructureName()).getConnection();
			FilledForm filledForm = getFormById(conn, formId);
			_log.info("filledForm exists Trying UPDATE FORM ON DB");
			boolean user_accepted = false;
			setFormProcessedAndStateById(conn, formId, user_accepted);
			_log.info("UPDATED FORM ON DB to Rejected, formId="+formId);

			_log.info("Sending email to user");
			Properties props = System.getProperties();
			Session session = null;
			props.put("mail.smtp.host", MConstants.MAIL_SERVICE_HOST);
			props.put("mail.smtp.port", MConstants.MAIL_SERVICE_PORT);
			//use localhost (probaly postfix instance)
			session = Session.getDefaultInstance(props);
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(CompileForm.MAIL_FROM));
			message.setRecipients(
					Message.RecipientType.TO, InternetAddress.parse(filledForm.getEmailAddress()));
			message.setSubject(MConstants.MAIL_SUBJECT_FEEDBACK);
			message.setText(MConstants.MAIL_BODY_REJECT);
			// Send message
			Transport.send(message);
			_log.info("Sending Rejection email done");

		} catch (Exception e) {
			_log.error("Something wrong with Databaseconnection or with getting FORMS from DB", e);
		}		
	}

	public void showAnswers(ActionRequest request, ActionResponse response) throws Exception {
		String formId = ParamUtil.getString(request, "applicationItem");
		Connection conn = null;
		try {
			conn = DatabaseConnection.getInstance("/"+PortalContext.getConfiguration().getInfrastructureName()).getConnection();
			_log.info("Trying getting FORMS from DB");
			FilledForm filledForm = getFormById(conn, formId);
			request.setAttribute("filledForm", filledForm);
			response.setWindowState(WindowState.MAXIMIZED);
			response.setRenderParameter("jspPage", "/html/manageforms/show_all_answers.jsp");
		} catch (Exception e) {
			_log.error("Something wrong with Databaseconnection or with getting FORMS from DB", e);
		}		

	}

	private static List<FilledForm> getNonProcessedForms(Connection conn) throws Exception {
		_log.debug("getting forms from DB ");
		List<FilledForm> toReturn = new ArrayList<>();
		String selectSQL = "SELECT * FROM forms WHERE processed_form = false";
		PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
		ResultSet rs = preparedStatement.executeQuery();
		while (rs.next()) {
			long id = rs.getLong("id");
			String name= rs.getString("name");
			String surname= rs.getString("surname");
			String organisation= rs.getString("organisation");
			String organisationType = rs.getString("organisation_type");
			String emailAddress = rs.getString("email");
			String areaOfExpertise = rs.getString("area_of_expertise");
			String country= rs.getString("country");
			boolean participatedInActivities = rs.getBoolean("activities_participation");
			String mainMotivation= rs.getString("main_motivation");
			String textareaMotivation = rs.getString("elaborated_motivation");
			String elaborated_expertise = rs.getString("elaborated_expertise");
			String degree_of_participation = rs.getString("degree_of_participation");
			boolean data_management = rs.getBoolean("data_management");
			Timestamp dateCreated = rs.getTimestamp("datecreated");
			boolean processed_form = rs.getBoolean("processed_form");
			boolean form_accepted = rs.getBoolean("user_accepted");
			FilledForm toAdd = new FilledForm(id, name, surname, organisation, organisationType, emailAddress, 
					areaOfExpertise, country, participatedInActivities, mainMotivation, textareaMotivation, 
					elaborated_expertise, degree_of_participation, data_management, dateCreated, processed_form, form_accepted);
			_log.info("Adding non processed form" + toAdd);
			toReturn.add(toAdd);
		}
		return toReturn;
	}


	private boolean setFormProcessedAndStateById(Connection conn, String formId, boolean user_accepted) {
		try {
			_log.debug("setFormProcessedAndStateById");	
			String selectSQL = "UPDATE forms SET processed_form = ?, user_accepted = ? WHERE id = ? ";
			PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
			long idToLookFor = Long.parseLong(formId);
			preparedStatement.setBoolean(1, true);
			preparedStatement.setBoolean(2, user_accepted);
			preparedStatement.setLong(3, idToLookFor);
			int result = preparedStatement.executeUpdate();
			if (result == 1)
				return true;
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}



	private static FilledForm getFormById(Connection conn, String formId) throws Exception {
		_log.debug("getting form by id from DB ");	
		String selectSQL = "SELECT * FROM forms WHERE id = ? ";
		PreparedStatement preparedStatement = conn.prepareStatement(selectSQL);
		long idToLookFor = Long.parseLong(formId);
		preparedStatement.setLong(1, idToLookFor);
		ResultSet rs = preparedStatement.executeQuery();
		while (rs.next()) {
			long id = rs.getLong("id");
			String name= rs.getString("name");
			String surname= rs.getString("surname");
			String organisation= rs.getString("organisation");
			String organisationType = rs.getString("organisation_type");
			String emailAddress = rs.getString("email");
			String areaOfExpertise = rs.getString("area_of_expertise");
			String country= rs.getString("country");
			boolean participatedInActivities = rs.getBoolean("activities_participation");
			String mainMotivation= rs.getString("main_motivation");
			String textareaMotivation = rs.getString("elaborated_motivation");
			String elaborated_expertise = rs.getString("elaborated_expertise");
			String degree_of_participation = rs.getString("degree_of_participation");
			boolean data_management = rs.getBoolean("data_management");
			Timestamp dateCreated = rs.getTimestamp("datecreated");
			boolean processed_form = rs.getBoolean("processed_form");
			boolean form_accepted = rs.getBoolean("user_accepted");
			FilledForm toAdd = new FilledForm(id, name, surname, organisation, organisationType, emailAddress, 
					areaOfExpertise, country, participatedInActivities, mainMotivation, textareaMotivation, 
					elaborated_expertise, degree_of_participation, data_management, dateCreated, processed_form, form_accepted);
			_log.info("Adding non processed form" + toAdd);
			return toAdd;
		}
		return null;
	}
}
