package org.gcube.portlets.user.moving.createaccounts;

import org.gcube.portal.event.publisher.lr62.PortalEvent;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Group;

public class CreateUserAddToVre extends PortalEvent {

    private static final long serialVersionUID = 1499288552188273747L;

    public static final String NAME = "create-user-add-to-vre";
    public static final String FIRST_NAME_ENTRY = "first-name";
    public static final String LAST_NAME_ENTRY = "last-name";
    public static final String EMAIL_ENTRY = "email";
    public static final String PASSWORD_ENTRY = "password";

    private CreateUserAddToVre(String username, String firstname, String lastname, String email, String password, Group group)
            throws PortalException, SystemException {

        super(NAME);
        setUser(username);
        setFirstname(firstname);
        setLastname(lastname);
        setEmail(email);
        setPassword(password);
        setGroup(group);
    }

    public static CreateUserAddToVre newEvent(String username, String firstname, String lastname, String email, String password,
            Group group) {

        try {
            return new CreateUserAddToVre(username, firstname, lastname, email, password, group);
        } catch (PortalException | SystemException e) {
            log.error("Cannot create event from group model object", e);
            return null;
        }
    }

  
    public void setFirstname(String firstname) {
        set(FIRST_NAME_ENTRY, firstname);
    }

    public String getFirstname() {
        return (String) get(FIRST_NAME_ENTRY);
    }

    public void setLastname(String lastname) {
        set(LAST_NAME_ENTRY, lastname);
    }

    public String getLastname() {
        return (String) get(LAST_NAME_ENTRY);
    }

    public void setEmail(String email) {
        set(EMAIL_ENTRY, email);
    }

    public String getEmail() {
        return (String) get(EMAIL_ENTRY);
    }

    public void setPassword(String password) {
        set(PASSWORD_ENTRY, password);
    }

    public String getPassword() {
        return (String) get(PASSWORD_ENTRY);
    }

}
