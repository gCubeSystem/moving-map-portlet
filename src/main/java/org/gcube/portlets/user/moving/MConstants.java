package org.gcube.portlets.user.moving;

public class MConstants {

	public static final String MAIL_SERVICE_HOST = "localhost";
	public static final String MAIL_SERVICE_PORT = "25";
	
	public static final String MAIL_SUBJECT_FEEDBACK = "MOVING EU Multi-Actor Platform: Expression of Interest application feedback";
	
	public static final String MAIL_BODY_REJECT = "Dear Sir/Madam,\n"
			+ "\n"
			+ "After a careful review and consideration of your expression of interest to join the MOVING EU-level Multi-Actor Platform (MAP), we regret to inform you that your request has not been granted. \n"
			+ "\n"
			+ "The MOVING EU MAP aims to form a dynamic and relevant community, ensuring a balanced representation of actors at different levels and covering a diversity of topics and knowledge relevant to the core objective of the project.\n"
			+ "\n"
			+ "If you would like to keep up to date with MOVING, please subscribe to our newsletter (https://mailchi.mp/4f25e203112c/movingnewsletter) or follow us online.\n"
			+ "\n"
			+ "Thank you for your interest.\n"
			+ "\n"
			+ "Best regards,\n"
			+ "MOVING team\n"
			+ "";
	
	public static final String MAIL_BODY_ACCEPT = "Dear Sir/Madam,\n"
			+ "\n"
			+ "After a careful review and consideration of your expression of interest to join the MOVING EU-level Multi-Actor Platform (MAP), we are happy to inform you that your request has been accepted.\n"
			+ "\n"
			+ "You will soon receive an email with link to access the EU MAP set up on the Virtual Research Environment (VRE). \n"
			+ "\n"
			+ "Best regards,\n"
			+ "MOVING team\n";

}
