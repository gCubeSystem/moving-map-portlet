package org.gcube.portlets.user.moving;

import java.io.Serializable;
import java.sql.Timestamp;

@SuppressWarnings("serial")
public class FilledForm implements Serializable {

	private long id;
	private String name;
	private String surname;
	private String organisation;
	private String organisationTyp;
	private String emailAddress;
	private String areaOfExpertise;
	private String country;
	private boolean participatedInActivities;
	private String mainMotivation;
	private String textareaMotivation;
	private String elaborated_expertise;
	private String degree_of_participation;
	private boolean data_management;
	private Timestamp dateCreated;
	private boolean processed_form;
	private boolean user_accepted;
	
	

	public FilledForm(long id, String name, String surname, String organisation, String organisationTyp,
			String emailAddress, String areaOfExpertise, String country, boolean participatedInActivities,
			String mainMotivation, String textareaMotivation, String elaborated_expertise,
			String degree_of_participation, boolean data_management, Timestamp dateCreated, boolean processed_form,
			boolean user_accepted) {
		super();
		this.id = id;
		this.name = name;
		this.surname = surname;
		this.organisation = organisation;
		this.organisationTyp = organisationTyp;
		this.emailAddress = emailAddress;
		this.areaOfExpertise = areaOfExpertise;
		this.country = country;
		this.participatedInActivities = participatedInActivities;
		this.mainMotivation = mainMotivation;
		this.textareaMotivation = textareaMotivation;
		this.elaborated_expertise = elaborated_expertise;
		this.degree_of_participation = degree_of_participation;
		this.data_management = data_management;
		this.dateCreated = dateCreated;
		this.processed_form = processed_form;
		this.user_accepted = user_accepted;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getOrganisation() {
		return organisation;
	}

	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}

	public String getOrganisationTyp() {
		return organisationTyp;
	}

	public void setOrganisationTyp(String organisationTyp) {
		this.organisationTyp = organisationTyp;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getAreaOfExpertise() {
		return areaOfExpertise;
	}

	public void setAreaOfExpertise(String areaOfExpertise) {
		this.areaOfExpertise = areaOfExpertise;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public boolean isParticipatedInActivities() {
		return participatedInActivities;
	}

	public void setParticipatedInActivities(boolean participatedInActivities) {
		this.participatedInActivities = participatedInActivities;
	}

	public String getMainMotivation() {
		return mainMotivation;
	}

	public void setMainMotivation(String mainMotivation) {
		this.mainMotivation = mainMotivation;
	}

	public String getTextareaMotivation() {
		return textareaMotivation;
	}

	public void setTextareaMotivation(String textareaMotivation) {
		this.textareaMotivation = textareaMotivation;
	}

	public String getElaborated_expertise() {
		return elaborated_expertise;
	}

	public void setElaborated_expertise(String elaborated_expertise) {
		this.elaborated_expertise = elaborated_expertise;
	}

	public String getDegree_of_participation() {
		return degree_of_participation;
	}

	public void setDegree_of_participation(String degree_of_participation) {
		this.degree_of_participation = degree_of_participation;
	}

	public boolean isData_management() {
		return data_management;
	}

	public void setData_management(boolean data_management) {
		this.data_management = data_management;
	}

	public boolean isProcessed_form() {
		return processed_form;
	}

	public void setProcessed_form(boolean processed_form) {
		this.processed_form = processed_form;
	}

	public boolean isUser_accepted() {
		return user_accepted;
	}

	public void setUser_accepted(boolean user_accepted) {
		this.user_accepted = user_accepted;
	}
	

	public Timestamp getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Timestamp dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String toString() {
		return "FilledForm [id=" + id + ", name=" + name + ", surname=" + surname + ", organisation=" + organisation
				+ ", organisationTyp=" + organisationTyp + ", emailAddress=" + emailAddress + ", areaOfExpertise="
				+ areaOfExpertise + ", country=" + country + ", participatedInActivities=" + participatedInActivities
				+ ", mainMotivation=" + mainMotivation + ", textareaMotivation=" + textareaMotivation
				+ ", elaborated_expertise=" + elaborated_expertise + ", degree_of_participation="
				+ degree_of_participation + ", data_management=" + data_management + ", dateCreated=" + dateCreated
				+ ", processed_form=" + processed_form + ", user_accepted=" + user_accepted + "]";
	}

	
	

}
